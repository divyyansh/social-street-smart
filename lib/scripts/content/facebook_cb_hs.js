
function facebook_clickbait(node) {

  const images = [...node.getElementsByClassName('mbs _6m6 _2cnj _5s6c')];

  images.forEach(function (el) {
    var links = el.getElementsByTagName('a');
    var link = '';
    for (var i = 0; i < links.length; i++) {
      link = (links[i].innerHTML);
    }

    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
      if (request.readyState === 4) {
        if (request.status === 200) {

          var data = JSON.parse(request.responseText);
          var clickbait_probability = data.Result;

          // Clickbait element. Append in parent element
          var clickbaitElement = document.createElement('div');
          clickbaitElement.classList.add('container');
          clickbaitElement.style.width = '22 rem';

          // Append in clickbaitElement
          var baitCard = document.createElement('div');
          baitCard.style.width = '18 rem';
          baitCard.classList.add('card');

          // Append in bait card
          var cardBody = document.createElement('div');
          cardBody.classList.add('card-body');

          // Append in card body
          var cardHeader = document.createElement('h5');
          cardHeader.classList.add('card-title');

          // Append in card body
          var progressContainer = document.createElement('div');
          progressContainer.classList.add('progress');

          // Append in progress container
          var progressBar = document.createElement('div');
          progressBar.classList.add('progress-bar');
          progressBar.style.width = Math.round(clickbait_probability*100) + '%';
          progressBar.textContent = Math.round(clickbait_probability*100) + '%';

          if(clickbait_probability > 0.9){
            cardHeader.classList.add('text-danger');
            cardHeader.textContent = 'This is a CLICKBAIT!';
            progressBar.classList.add('bg-danger');
          }
          else{
            cardHeader.classList.add('text-primary');
            cardHeader.textContent = 'Clickbait Chance';
            progressBar.classList.add('bg-primary');
          }

          var elParent = el.parentNode;
          var parentElement = elParent.parentNode;

          progressContainer.append(progressBar);
          cardBody.append(cardHeader);
          cardBody.append(progressContainer);
          baitCard.append(cardBody);
          clickbaitElement.append(baitCard);
          parentElement.classList.add('container');
          parentElement.style.width = '22rem';
          parentElement.appendChild(clickbaitElement);
          //console.log(clickbait_banner);
          //console.log(nodeParent);
        }
      }
    };

    request.open('GET', 'http://127.0.0.1:5000/pred?text=' + link, true);
    request.send();
  });

};

function facebook_hatespeech(node) {

  const images = [...node.getElementsByClassName('_5pbx userContent _3576')];

  images.forEach(function (el) {
    //  var links = el.getElementsByTagName('a');
    var link = '';
    link = el.textContent;

    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
      if (request.readyState === 4) {
        if (request.status === 200) {
          var data = JSON.parse(request.responseText);
          var toxic_probability = data.Toxic;

          // Fake News element. Append in parent element
          var fakeNewsElement = document.createElement('div');
          fakeNewsElement.classList.add('container');
          fakeNewsElement.style.width = '22 rem';

          // Append in fakeNewsElement
          var baitCard = document.createElement('div');
          baitCard.style.width = '18 rem';
          baitCard.classList.add('card');


          // Append in bait card
          var cardBody = document.createElement('div');
          cardBody.classList.add('card-body');

          // Append in card body
          var cardHeader = document.createElement('h5');
          cardHeader.classList.add('card-title');

          // Append in card body
          var progressContainer = document.createElement('div');
          progressContainer.classList.add('progress');

          // Append in progress container
          var progressBar = document.createElement('div');
          progressBar.classList.add('progress-bar');
          progressBar.style.width = Math.round(toxic_probability*100) + '%';
          progressBar.textContent = Math.round(toxic_probability*100) + '%';

          if(toxic_probability > 0.9){
            cardHeader.classList.add('text-danger');
            cardHeader.textContent = 'FAKE NEWS!';
            progressBar.classList.add('bg-danger');
          }
          else{
            cardHeader.classList.add('text-primary');
            cardHeader.textContent = 'Chances of Fake News';
            progressBar.classList.add('bg-primary');
          }

          var elParent = el.parentNode;
          var parentElement = elParent.parentNode;
          progressContainer.append(progressBar);
          cardBody.append(cardHeader);
          cardBody.append(progressContainer);
          baitCard.append(cardBody);
          fakeNewsElement.append(baitCard);
          parentElement.classList.add('container');
          parentElement.style.width = '22rem';
          parentElement.appendChild(fakeNewsElement);
          //console.log(clickbait_banner);
          //console.log(nodeParent);
        }
      }
    };

    request.open('GET', 'http://127.0.0.1:5000/pred?text=' + link, true);
    request.send();

  });

};

/**
const observer = new MutationObserver(function (mutations) {
  mutations.forEach(function (mutation) {
    mutation.addedNodes.forEach(function (node) {
      if (node.nodeType === 1) { // ELEMENT_NODE
        facebook_clickbait(node);
      }
    });
  });
});

const config = {
  attributes: false,
  childList: true,
  characterData: false,
  subtree: true
};

observer.observe(document.body, config);

**/