function twitter_clickbait(node) {

  // console.log("in function");
  const images = [...node.getElementsByClassName('css-901oao css-cens5h r-1re7ezh r-1qd0xha r-a023e6 r-16dba41 r-ad9z0x r-bcqeeo r-qvutc0')];

  images.forEach(function (el) {
    var headline_span = el.getElementsByTagName('span');

    var headline = headline_span[0].innerText;
    //console.log("in function");



    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
      if (request.readyState === 4) {
        if (request.status === 200) {
          var data = JSON.parse(request.responseText);
          var clickbait_probability = data.Result;

          // Append in parentElement
          var baitCard = document.createElement('div');
          baitCard.style.width = '150px';
          baitCard.style.height = '70px';
          baitCard.classList.add('card');

          // Append in bait card
          var cardBody = document.createElement('div');
          cardBody.classList.add('card-body');

          // Append in card body
          var cardHeader = document.createElement('h5');
          cardHeader.classList.add('card-title');

          // Append in card body
          var progressContainer = document.createElement('div');
          progressContainer.classList.add('progress');

          // Append in progress container
          var progressBar = document.createElement('div');
          progressBar.classList.add('progress-bar');
          progressBar.style.width = Math.round(clickbait_probability * 100) + '%';
          progressBar.textContent = Math.round(clickbait_probability * 100) + '%';

          if (clickbait_probability > 0.9) {
            cardHeader.classList.add('text-danger');
            cardHeader.textContent = 'This is a CLICKBAIT!';
            progressBar.classList.add('bg-danger');
          } else {
            cardHeader.classList.add('text-primary');
            cardHeader.textContent = 'Clickbait Chance';
            progressBar.classList.add('bg-primary');
          }
          var elParent = el.parentNode;
          var parentParent= elParent.parentNode;

          progressContainer.append(progressBar);
          cardBody.append(cardHeader);
          cardBody.append(progressContainer);
          baitCard.append(cardBody);
          parentParent.append(cardBody);
        //  console.log(clickbait_banner);
        //  console.log(x);
        }
      }
    };

    request.open('GET', 'http://127.0.0.1:5000/pred?text=' + headline, true);
    request.send();
	 																																																																																								
  });

};


function twitter_hatespeech(node) {

  const images = [...node.getElementsByClassName('css-901oao r-hkyrab r-1qd0xha r-a023e6 r-16dba41 r-ad9z0x r-bcqeeo r-bnwqim r-qvutc0')];

  images.forEach(function (el) {
  //  var links = el.getElementsByTagName('a');
    var link = '';
    link = el.textContent;

    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
      if (request.readyState === 4) {
        if (request.status === 200) {
          var data = JSON.parse(request.responseText);
          var toxic_probability = data.Toxic;

          // Append in Main Parent element
          var baitCard = document.createElement('div');
          baitCard.style.width = '150px';
          baitCard.style.height = '70px';
          baitCard.classList.add('card');

          // Append in bait card
          var cardBody = document.createElement('div');
          cardBody.classList.add('card-body');

          // Append in card body
          var cardHeader = document.createElement('h5');
          cardHeader.classList.add('card-title');

          // Append in card body
          var progressContainer = document.createElement('div');
          progressContainer.classList.add('progress');

          // Append in progress container
          var progressBar = document.createElement('div');
          progressBar.classList.add('progress-bar');
          progressBar.style.width = Math.round(toxic_probability * 100) + '%';
          progressBar.textContent = Math.round(toxic_probability * 100) + '%';

          if (toxic_probability > 0.9) {
            cardHeader.classList.add('text-danger');
            cardHeader.textContent = 'FAKE NEWS!';
            progressBar.classList.add('bg-danger');
          } else {
            cardHeader.classList.add('text-primary');
            cardHeader.textContent = 'Chances of Fake News';
            progressBar.classList.add('bg-primary');
          }
          var elParent = el.parentNode;
          var parentParent= elParent.parentNode;
          progressContainer.append(progressBar);
          cardBody.append(cardHeader);
          cardBody.append(progressContainer);
          baitCard.append(cardBody);
          parentParent.append(cardBody);
          //console.log(clickbait_banner);
          //console.log(nodeParent);
        }
      }
    };

    request.open('GET', 'http://127.0.0.1:5000/pred?text=' + link, true);
    request.send();
	 																																																																																								
  });

};

/**

const observer = new MutationObserver(function (mutations) {
  mutations.forEach(function (mutation) {
    mutation.addedNodes.forEach(function (node) {
      if (node.nodeType === 1) { // ELEMENT_NODE
        facebook_clickbait(node);
      }
    });
  });
});

const config = {																																																																																															
  attributes: false,
  childList: true,
  characterData: false,
  subtree: true
};

observer.observe(document.body, config);
**/
																				
